# HF Tracheal V1

HF_Tracheal_V1 contains tracheal sound audio files collected from 227 subjects during a procedure under non-intubated intravenous conscious sedation. A related academic paper can be found at https://www.sciencedirect.com/science/article/pii/S1746809423006559.

The sound recording is approved by the following Institutional Review Boards (IRBs), including the Joint Institutional Review Board of the Medical Research Ethical Foundation, Taipei, Taiwan (case number: 19-006-A-2), IRB of Taipei Tzu Chi Hospital, Buddhist Tzu Chi Medical Foundation (case number: 09-XD-079), IRB of En Chu Kong Hospital (case number: ECKIRB1090303), IRB of Taipei City Hospital (case number: TCHIRB-10811009-E), and IRB of National Taiwan University Hospital (case numbers: 201811991DIND and 202005108DIPC).

One hundred and ten of the subjects were men and 116 were women. The sex of one subject was unknown. Two hundred and nine subjects underwent gastrointestinal (GI) endoscopy, 1 subject took laparoscopy, 8 experienced Cesarean section, and 9 had obstructive sleep apnea-related procedures. 

The tracheal sound of 108 subjects was recorded with the HF-Type-2 device, which is composed of an electronic stethoscope (AccurSound-101, Heroic Faith, Taipei, Taiwan) connected to a smartphone. 

The tracheal sound of the other 119 subjects was recorded with the HF-Type-3 device, which is composed of a conventional chestpiece, a stethoscope tubing, a microphone, and a smartphone. 

The initial prefix "trunc_" indicates the 15-second files are truncated from their original recordings. Following the initial prefix, the filename created by HF-Type-2 has a second prefix of "type2_" and it created by HF-Type-3 has a second prefix of "airm_". After the second prefix follows a datetime string in a format like "yyyymmdd_HHMMss" though the datetime is modified for the de-link purpose. The ending of the filename indicates the sequential number of the truncation where HF-Type-2 creates a sequential number in a format of "%08d" and HF-Type-3 has a format of "%d". 

The audio files in the HF_Tracheal_V1 are licensed under an Attribution-NonCommercial 4.0 (CC BY-NC 4.0) International License. 

The labels are proprietary property owned by Heroic Faith Medical Science Co. Ltd., Taipei, Taiwan. Access to the labels must be granted by the board of directors of Heroic Faith, and the application form can be found at https://forms.gle/YwEbPDUuNpByP6RW7.

Note that the management of the database will soon be transferred to Taiwan AI Lab and the access link will be changed and upated.
